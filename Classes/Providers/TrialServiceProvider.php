<?php

namespace Mtc\Plugins\Trial\Classes\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Mtc\Plugins\Trial\Classes\Console\Commands\CopyRemoteRoomsCommand;

/**
 * Class TrialServiceProvider
 *
 * Here we link up all functionality that is added by this plugin
 * You can create it by running `php artisan make:provider ExampleServiceProvider`
 *
 * @package Mtc\Plugins\Classes\Providers
 */
class TrialServiceProvider extends ServiceProvider
{
    /**
     * Register the functionality
     */
    public function register()
    {
        $this->mergeConfigFrom(dirname(__DIR__, 2) . '/config/trial.php', 'trial');

        // Registering package commands.
        if ($this->app->runningInConsole()) {
            $this->commands([
                CopyRemoteRoomsCommand::class,
            ]);
        }
    }

    /**
     * Boot (Start) the functionality
     */
    public function boot()
    {
        if (config('trial.enabled') !== true) {
            return;
        }

        $this->loadMigrationsFrom(dirname(__DIR__, 2) . '/database/migrations');
        Route::middleware('web')->group(dirname(__DIR__, 2) . '/routes/web.php');
        Route::middleware('api')->group(dirname(__DIR__, 2) . '/routes/api.php');
    }
}