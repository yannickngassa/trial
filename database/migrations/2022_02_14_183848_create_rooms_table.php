<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trial_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('ext_id');
            $table->string('address')
                ->nullable();
            $table->text('description')
                ->nullable();
            $table->string('image')
                ->nullable();
            $table->string('thumbnail')
                ->nullable();
            $table->float('latitude');
            $table->float('longitude');
            $table->smallInteger('nb_bedrooms')
                ->nullable()
                ->unsigned();
            $table->smallInteger('nb_bathrooms')
                ->nullable()
                ->unsigned();
            $table->integer('price')
                ->unsigned();
            $table->smallInteger('type')
                ->unsigned();
            $table->integer('county_id')
                ->unsigned();
            $table->integer('property_id')
                ->unsigned();
            $table->foreign('county_id')
                ->references('id')
                ->on('trial_counties')
                ->onDelete('cascade');
            $table->foreign('property_id')
                ->references('id')
                ->on('trial_properties')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
