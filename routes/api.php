<?php

use Illuminate\Routing\Router;
use Mtc\Plugins\Trial\Classes\Http\Controllers\{TownControllerAjax,RoomControllerAjax};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** @var Router $router */

$router->group([
    'prefix'    => 'api'
], function(Router $router) {
    $router->resource('towns', TownControllerAjax::class)->only(['index']);
    $router->resource('rooms', RoomControllerAjax::class)->only(['index']);
});

