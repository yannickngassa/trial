Vue.component('trial-filter', require('./TrialFilterComponent').default);

$(function () {
    const baseUrl = window.location.pathname || '/';
    $(document).on('trial.filter:update', window.debouncer(function (e, params) {
        if(typeof window.doAjax === 'boolean' && !window.doAjax) {
            return;
        }

        $.ajax({
            url: baseUrl,
            method: 'GET',
            dataType: 'html',
            data: params,
            beforeSend: function () {
                window.doAjax = false;
                $('#loadingOverlay').fadeIn(500);
            }
        })
            .done(function (res) {
                const url = Object.entries(params).reduce((res, entry) => {
                    return entry[1] ? `${res}&${entry[0]}=${entry[1].toString()}` : res
                }, baseUrl).replace('&', '?');

                $('#trialContent').html($(res).find('#trialContent').html());
                window.history.pushState({'pageTitle': document.title, 'response': res}, '', url);
            })
            .fail(function () {
                alert('Something wrong happened, please try again later!');
            })
            .always(function () {
                doAjax = true;
                $('#loadingOverlay').fadeOut(500);
            })
    }, 1000))
});