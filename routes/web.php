<?php
/**
 * This is a route file
 * Here you can define plugin specific routes
 */

use Illuminate\Routing\Router;
use Mtc\Plugins\Trial\Classes\Http\Controllers\RoomController;

/** @var Router $router */

$router->resource(config('trial.path'), RoomController::class)->only(['index']);