<?php

namespace Mtc\Plugins\Trial\Classes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static firstOrCreate(string[] $array, int[] $array1)
 */
class County extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = 'trial_counties';

    protected $fillable = [
        'name',
        'town_id'
    ];

    /**
     * Get the town of the county
     *
     * @return BelongsTo
     */
    public function town(): BelongsTo
    {
        return $this->belongsTo(Town::class);
    }
}
