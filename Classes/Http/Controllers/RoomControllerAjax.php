<?php

namespace Mtc\Plugins\Trial\Classes\Http\Controllers;

use Mtc\Core\Http\Controllers\Controller;
use Mtc\Plugins\Trial\Classes\Http\Resources\RoomResource;
use Mtc\Plugins\Trial\Classes\Room;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class RoomControllerAjax extends Controller
{
    public function index(Request $request): AnonymousResourceCollection
    {
        $filterDefaults = [
            'towns'         => $request->input('town'),
            'nb_bedrooms'   => $request->input('nb_bedrooms'),
            'type'          => $request->input('type'),
            'price'         => [
                'from'      => $request->input('price.from'),
                'to'        => $request->input('price.to')
            ]
        ];
        $rooms = Room::when($filterDefaults['type'], function ($q, $v) {
            $q->where('type', $v);
        })
            ->when($filterDefaults['nb_bedrooms'], function ($q, $v) {
                $q->where('nb_bedrooms', $v);
            })
            ->when($filterDefaults['price'], function ($q, $v){
                if($v['from'] && $v['to']) {
                    $q->whereBetween('price', $v);
                } else {
                    $q->where('price', empty($v['to']) ? '>=' : '<=', $v['from'] ?? $v['to'] ?? 0);
                }
            })
            ->when(!empty($filterDefaults['towns']), function ($q)  use ($filterDefaults) {
                $q->whereHas('county.town', function ($q) use ($filterDefaults) {
                    $q->whereIn('id', $filterDefaults['towns']);
                });
            })
            ->paginate(15);

        return RoomResource::collection($rooms);
    }
}
