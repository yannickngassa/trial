<?php

namespace Mtc\Plugins\Trial\Classes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{HasMany,HasManyThrough};

/**
 * @method static firstOrCreate(string[] $array)
 */
class Country extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = 'trial_countries';

    protected $fillable = [
        'name'
    ];

    /**
     * Get all towns of the country
     *
     * @return HasMany
     */
    function towns(): HasMany
    {
        return $this->hasMany(Town::class);
    }

    /**
     * Get all cities of the country
     *
     * @return HasManyThrough
     */
    public function counties(): HasManyThrough
    {
        return $this->hasManyThrough(County::class, Town::class);
    }
}
