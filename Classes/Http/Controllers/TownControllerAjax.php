<?php

namespace Mtc\Plugins\Trial\Classes\Http\Controllers;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Mtc\Plugins\Trial\Classes\Http\Resources\TownResource;
use Mtc\Plugins\Trial\Classes\Town;
use Mtc\Core\Http\Controllers\Controller;

class TownControllerAjax extends Controller
{
    function index(): AnonymousResourceCollection
    {
        return TownResource::collection(Town::all());
    }
}
