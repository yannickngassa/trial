<?php

namespace Mtc\Plugins\Trial\Classes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static firstOrCreate(string[] $array, array $array1)
 */
class Town extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = 'trial_towns';
    protected $fillable = [
        'name',
        'country_id'
    ];

    /**
     * Get the country of the town
     *
     * @return BelongsTo
     */
    function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get all counties of the town
     *
     * @return HasMany
     */
    function counties(): HasMany
    {
        return $this->hasMany(County::class);
    }
}
