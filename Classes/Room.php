<?php

namespace Mtc\Plugins\Trial\Classes;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static when(mixed $type, Closure $param)
 * @method static firstOrCreate(array $array, array $array1)
 */
class Room extends Model
{
    use SoftDeletes;

    public const FOR_SALE = 1;
    public const FOR_RENT = 2;

    protected $table = 'trial_rooms';
    protected $attributes = [
        'type' => self::FOR_RENT,
    ];
    protected $casts = [
    ];
    protected $fillable = [
        'ext_id',
        'address',
        'description',
        'latitude',
        'longitude',
        'image',
        'thumbnail',
        'price',
        'nb_bedrooms',
        'nb_bathrooms',
        'property_id',
        'county_id',
        'type',
    ];
    /**
     * @var int
     */
    private $type;

    function setTypeAttribute(int $type): void
    {
        switch ($this->attributes['type']) {
            case self::FOR_RENT:
            case self::FOR_SALE:
                $this->type = $type;
                break;
            default:
                throw new \ValueError();
        }
    }

    /**
     * Get the county of the room
     *
     * @return BelongsTo
     */
    function county(): BelongsTo
    {
        return $this->belongsTo(County::class);
    }

    /**
     * Get the property of the room
     *
     * @return BelongsTo
     */
    function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }

    static function getTypes(): array
    {
        return [
            Room::FOR_RENT  => 'Rent',
            Room::FOR_SALE  => 'Sale'
        ];
    }
}
