<?php

namespace Mtc\Plugins\Trial\Classes;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static firstOrCreate(int[] $array, string[] $array1)
 */
class Property extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = 'trial_properties';

    protected $fillable = [
        'ext_id',
        'name',
        'description'
    ];
}
