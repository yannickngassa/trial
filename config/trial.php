<?php

return [
    'enabled'   => true,
    'api_url'   => 'https://trial.craig.mtcserver15.com/api',
    'api_key'   => '',
    'per_page'  => 16,
    'path'      => '/rooms'
];