<?php

namespace Mtc\Plugins\Trial\Classes\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int $id
 * @property float $latitude
 * @property float $longitude
 * @property string $address
 * @property int $price
 * @property string $image
 * @property string $thumbnail
 * @property int $nb_bedrooms
 * @property int $nb_bathrooms
 * @property int $type
 * @property string $description
 */
class RoomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'            => $this->id,
            'coordinates'   => [$this->latitude, $this->longitude],
            'address'       => $this->address,
            'price'         => $this->price,
            'image'         => [
                'original'  => $this->image,
                'thumbnail' => $this->thumbnail
            ],
            'nb_bedrooms'   => $this->nb_bedrooms,
            'nb_bathrooms'  => $this->nb_bathrooms,
            'type'          => $this->type,
            'description'   => $this->description
        ];
    }
}
