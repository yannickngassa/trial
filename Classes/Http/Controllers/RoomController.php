<?php

namespace Mtc\Plugins\Trial\Classes\Http\Controllers;

use Mtc\Core\Http\Controllers\Controller;
use Mtc\Core\PaginationTemplate;
use Illuminate\Http\Request;
use Mtc\Plugins\Trial\Classes\Room;
use Mtc\Plugins\Trial\Classes\Town;

class RoomController extends Controller
{
    public function index(Request $request)
    {
        $twig = \app('twig');
        $perPage = config('trial.per_page');
        $filterDefaults = [
            'towns'         => $request->input('town'),
            'nb_bedrooms'   => $request->input('nb_bedrooms'),
            'type'          => $request->input('type'),
            'price_from'      => $request->input('price_from'),
            'price_to'        => $request->input('price_to')
        ];
        $rooms = Room::when($filterDefaults['type'], function ($q, $v) {
            $q->where('type', $v);
        })
            ->when($filterDefaults['nb_bedrooms'], function ($q, $v) {
                $q->where('nb_bedrooms', $v);
            })
            ->when([
                'from' => $filterDefaults['price_from'],
                'to'    => $filterDefaults['price_to']
            ], function ($q, $v){
                if($v['from'] && $v['to']) {
                    $q->whereBetween('price', $v);
                } else {
                    $q->where('price', empty($v['to']) ? '>=' : '<=', $v['from'] ?? $v['to'] ?? 0);
                }
            })
            ->when(!empty($filterDefaults['towns']), function ($q)  use ($filterDefaults) {
                $q->whereHas('county.town', function ($q) use ($filterDefaults) {
                    $q->whereIn('id', $filterDefaults['towns']);
                });
            });
        $pagination = new PaginationTemplate([
            'item_count'    => $rooms->count(),
            'per_page'      => $perPage,
            'active_page'   => $request->input('page') or 1,
            'link_class'    => 'js_filterLink',
            'page_url'      => config('trial.path') . '?' . http_build_query($filterDefaults)
        ]);
        $list_html = $twig->render('trial/list.twig', [
            'rooms'             => $rooms->paginate($perPage),
            'pagination_html'   => $pagination->render($twig)
        ]);
        $filter_html = $twig->render('trial/filter.twig', [
            'defaults'      => $filterDefaults,
            'towns'         => Town::all(),
            'types'         => Room::getTypes()
        ]);
        $page_meta = [
            'title'         => 'Rooms',
            'page_title'    => 'Rooms',
        ];

        return template('trial/index.twig', [
            'page_meta'     => $page_meta,
            'list_html'     => $list_html,
            'filter_html'   => $filter_html,
        ]);
    }
}
